# nodelearner

A collection of machine learning algorithms in Javascript

At the minute, only DecisionTree is available, but more are coming soon.

*Note: use require('babel/register'), as the project uses ES6 features*

Example Usage:

```Javascript
require('babel/register');
var fs = require('fs');
var DecisionTree = require('nodelearner').DecisionTree;
var DataSet = require('nodelearner').DataSet;

//Import dataset and convert to array, removing trailing newline characters
var lenseData = fs.readFileSync('./lenses.data').toString();
lenseData = lenseData.replace(/\n$/, "").split('\n');

var dataSet = new DataSet();
var targets = [];
for(var line of lenseData) {
    line = line.match(/[0-9]+/g);
    var data = {
        "age": line[1],
        "prescription": line[2],
        "astigmatic": line[3],
        "tear_production_rate": line[4]
    };

    dataSet.add({
        "data": data,
        "target": line[5]
    });
    if(targets.indexOf(line[5]) < 0) {
        targets.push(line[5]);
    }
}
var learner = new DecisionTree();
learner.train(dataSet,targets);

var successes = 0;
var errors = 0;

dataSet.where = {};
for(let data of dataSet) {
    let target = data.target;
    if(learner.predict(data.data) === target) {
        successes++;
    } else {
        errors++;
    }
}

let successRate = successes/data.size() * 100;
    console.log("Success: " + successes, "Errors: " + errors, "Success rate: " + successRate );
}
```
