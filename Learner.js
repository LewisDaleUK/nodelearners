require('babel/register');
var fs = require('fs');
var DecisionTree = require('./Learners/DecisionTree');
var DataSet = require('./DataStructures/DataSet');

module.exports = {
    "DecisionTree": DecisionTree,
    "DataSet": DataSet
};
