'use strict'
let Tree = require('../DataStructures/Tree');
let Sort = require('node-sort');

/**
 * A Decision Tree learner implementation in ES6 - a useful machine learning algorithm for classification based on discrete values
 *
 * @author Lewis Dale
 */
class DecisionTree {
    constructor() {
        this.tree = new Tree();
    }
    /**
     * Train the decision tree using a given attribute-value dataset
     * @param data A DataSet object containing attribute-value data instances, labelled
     * @param targets An array of targets for the training data
     */
    train(data,targets) {
        this.tree = this.buildTree(data,targets,[]);


    }
    /**
     * Build a tree from the training data
     * @param data The training data supplied
     * @param targets The targets for the training data
     * @param found The attributes that have already been found
     * @return a Graph object containing the tree
     */
    buildTree(data,targets,found) {
        if(targets.length === 1) {
            return new Tree("Result", { value: targets[0] });
        } else if(targets.length > 1) {
            let attributes = this.collectAttributes(data,targets);

            for(let attribute of found) {
                attributes.delete(attribute);
            }

            let gains = this.calculateGainsSorted(attributes,data.size(),targets);
            let root = new Tree(gains[0].label, {}, []);
            found.push(root.label);

            let values = attributes.get(root.label).values;
            for(let value of values.entries()) {
                let child = new Tree(value[0]);
                let subData = data.clone().match(root.label,child.label);
                let subTargets = [];
                for(let item of subData) {
                    if(subTargets.indexOf(item.target) < 0) {
                        subTargets.push(item.target);
                    }
                }

                child.addChild(this.buildTree(subData,subTargets,found));
                root.addChild(child);
            }
            return root;
        }

    }
    /**
     * Collects all the attributes from the data set into a Map object
     * Formats the data to make it easier to process
     * @param data The data to format
     * @param targets The targets for the data
     * @return a Map object containing the formatted data
     */
    collectAttributes(data,targets) {
        let attributes = new Map();

        //this.calculateGainsSorted(attributes,data.length);
        for(var i = 0; i < data.size();i++) {
            var line = data.get(i).data;
            var target = data.get(i).target;

            //Iterate over each attribute
            for(let attr in line) {
                let val = line[attr];

                if(!attributes.has(attr)) {
                    let attribute = {
                        'values': new Map(),
                    };
                    attributes.set(attr,attribute);
                }

                let attrVals = attributes.get(attr);
                if(!attrVals.values.has(val)) {
                    let targetTotal = {};
                    for(let t of targets) {
                        targetTotal[t] = 0;
                    }
                    attrVals.values.set(val,targetTotal);
                }

                let value = attrVals.values.get(val);
                value[target]++;
                attrVals.values.set(val,value);
                attributes.set(attr,attrVals);
            }
        }
        return attributes;
    }
    /**
     * Calculates the entropy value for a given set of data
     * @param value The value of the entropy caluclation
     * @param size The total number of items in the set - necessary for weighting
     * @return float The entropy value
     */
    calculateEntropy(value,size) {
        if(value !== 0) {
            let probability = value / size;
            let entropy = probability * (-Math.log2(probability));
            return entropy;
        }
        return 0;
    }
    /**
     * Calculates the information gain for a set of data
     * @param results An object containing the reuslts data to calculate from
     * @param targets A list of the potential target values
     * @param size The total size of the data set
     * @return float The calculated entropies
     */
    calculateTotalEntropy(results,targets,size) {
        let entropy = 0;

        for(let target of targets) {
            let value = results[target];
            entropy += this.calculateEntropy(value,size);
        }

        return entropy;
    }
    /**
     * Calculate the information gains of a set of attributes and Sort
     * @param attributes a Map of the attributes with the relevant values and results
     * @param size The total size of the dataset
     * @param targetList A list of the possible target values
     * @return a sorted array of objects containing the attribute labels and the gain values
     */
    calculateGainsSorted(attributes, size, targetList) {
        let gains = [];

        for(let attribute of attributes.entries()) {
            //TODO: Calculate total entropy of the attribute
            //TODO: Calculate information gain for the attribute
            let label = attribute[0],
                values = attribute[1].values,
                totals = {};

            for(let target of targetList) {
                totals[target] = 0;
            }

            //Calculate the entropies for each value - while doing so, collect the data necessary to calculate total attribute entropy
            let gain = 0;
            for(let value of values.entries()) {

                let results = value[1],
                    entropy = this.calculateTotalEntropy(results,targetList,size),
                    count = 0;
                for(let target of targetList) {

                    let targetVal = results[target];
                    if(targetVal !== 0) {
                        totals[target] += targetVal;
                        count += targetVal;
                    }
                }
                gain -= (count/size) * entropy;

            }

            //Calculate total attribute entropy
            let attributeEntropy = this.calculateTotalEntropy(totals,targetList,size);
            let finalGain = attributeEntropy + gain;

            if(finalGain >= 0) {
                gains.push(
                    {
                        label: label,
                        gain: attributeEntropy + gain
                    }
                );
            }

        }

        let sorter = new Sort();

        let sorted = sorter.mergeSort(gains, function(one,two) {
            if(one.gain > two.gain) {
                return -1;
            } else if(one.gain < two.gain) {
                return 1;
            } else if(one.gain == two.gain) {
                return 0;
            }
        });
        return sorted;
    }

    /**
     * Predict the outcome of an instance
     * @param instance An instance object
     * @return prediction A value containing the prediction for the instance - this can be any type
     */
    predict(instance) {
        //TODO: Perform a prediction based on the tree
        return this.check(this.tree,instance);
    }
    /**
     * Check a branch node by finding the next iteration of the tree
     * @param node The node to check
     * @param data The data item to check
     */
    check(node,data) {
        if(node.label === "Result") {
            return node.data.value;
        }

        let attribute = node.label;
        let value = data[attribute];
        for(let child of node.children) {
            if(child.label === value) {
                return this.check(child.children[0],data);
            }
        }
    }
}
module.exports = DecisionTree;
