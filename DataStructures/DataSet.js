'use strict'
/**
 * An object to represent a dataset
 *
 * Queryable and chainable
 * @author Lewis Dale
 */
class DataSet {
    constructor() {
        this.data = [];
        this.where = {};
    }
    /**
     * Iterate through the data items in the set and return
     * Match with the queries while doing so
     */
    *[Symbol.iterator]() {
        iterator:
        for(let data of this.data) {
            for(let attribute in this.where) {
                if(data.data[attribute] !== this.where[attribute]) {
                    continue iterator;
                }
            }
            yield data;
        }
    }
    /**
     * Add an item to the data set
     * @param item The item to add
     * @return self For chaining
     */
     add(item) {
         if(item !== undefined) {
             this.data.push(item);
         }

         return this;
     }
     /**
      * Add a rule to match the data on
      * @param attribute The attribute to match
      * @param value The value to match
      * @return self For chaining
      */
     match(attribute, value) {
         this.where[attribute] = value;
         return this;
     }
     /**
      * Return a data item at a specific index
      * @param index The index of the data item
      * @return the data item if it exists
      * @return null if it does not
      */
     get(index) {
         if(this.data.length >= index) {
             return this.data[index];
         } else {
             return null;
         }
     }
     /**
      * Returns the size of the DataSet, taking into account the queries
      * @return the dataset size
      */
     size() {
         let size = 0;
         for(let item of this) {
             size++;
         }
         return size;
     }
     /**
      * Removes an item at a given index
      * @param index The index
      * @return self For chaining
      */
     remove(index) {
         if(this.data.length < index) {
             this.data.splice(index,1);
         }
         return this;
     }
     /**
      * Clones the DataSet object
      * @return a cloned copy of DataSet
      */
     clone() {
         let cloned = new DataSet();
         cloned.data = this.data;
         cloned.where = this.where;
         return cloned;
     }

}

module.exports = DataSet;
