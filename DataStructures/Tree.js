'use strict'
/**
 * Implementation of the Tree data type in Javascript
 * All of the implementations I could find on npm were sub-standard.
 *
 * This tree is recursive, so every node is also a Tree object
 * @author Lewis Dale
 */
class Tree {
    constructor(label, data, children) {
        this.children = [];
        this.label = label === undefined ? "" : label;
        this.data = data === undefined ? {} : data;

        if(children !== undefined &&
            {}.toString.call(children) === "[object Array]") {

            this.addChildren(children);
        }

    }
    /**
     * Add a node as the child of the current tree
     * @param node The node to add - must be a Tree object,
     * @return self For chaining of methods
     */
    addChild(node) {
        if(node instanceof Tree) {
            this.children.push(node);
        }

        return this;
    }
    /**
     * Add an array of child nodes
     * @param children An array of Child nodes
     * @return self For method chaining
     */
    addChildren(children) {
        if(children !== undefined && {}.toString.call(children) === "[object Array]") {
            for(let child of children) {
                this.addChild(child);
            }
        }

        return this;
    }
    /**
     * Searches the tree for a child node by label
     * @param label The label to search for
     * @return The child node if found
     * @return An empty tree if no child found
     */
    findChild(label) {
        for(let child of this) {
            if(child.label === label) {
                return child;
            }
        }
        return new Tree();
    }
    /**
     * Generator function
     * Yields each child of the current tree, using breadth-first search
     */
    *[Symbol.iterator]() {
        let queue = [];
        queue.push(this);
        while(queue.length !== 0) {
            let node = queue.shift();
            queue = queue.concat(node.children);

            yield node;
        }
    }
}

module.exports = Tree;
